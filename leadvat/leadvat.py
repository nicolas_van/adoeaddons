# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2014 InfiniDev (<http://infinidev.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.addons.base_vat import base_vat
import string
from openerp.tools.translate import _

class crm_lead(osv.osv):
    _inherit = "crm.lead"

    _columns = {
        'vat': fields.char('TIN', size=32, help="Tax Identification Number."),
    }

    def _lead_create_contact(self, cr, uid, lead, name, is_company, parent_id=False, context=None):
        val = super(crm_lead, self)._lead_create_contact(cr, uid, lead, name, is_company, parent_id, context)
        if lead.vat:
            self.pool.get("res.partner").write(cr, uid, val, {"vat": lead.vat}, context=context)
        return val

    def check_vat(self, cr, uid, ids, context=None):
        user_company = self.pool.get('res.users').browse(cr, uid, uid).company_id
        if user_company.vat_check_vies:
            # force full VIES online check
            check_func = self.pool.get("res.partner").vies_vat_check
        else:
            # quick and partial off-line checksum validation
            check_func = self.pool.get("res.partner").simple_vat_check
        for lead in self.browse(cr, uid, ids, context=context):
            if not lead.vat:
                continue
            vat_country, vat_number = self.pool.get("res.partner")._split_vat(lead.vat)
            if not check_func(cr, uid, vat_country, vat_number, context=context):
                return False
        return True

    def _construct_constraint_msg(self, cr, uid, ids, context=None):
        def default_vat_check(cn, vn):
            # by default, a VAT number is valid if:
            #  it starts with 2 letters
            #  has more than 3 characters
            return cn[0] in string.ascii_lowercase and cn[1] in string.ascii_lowercase
        vat_country, vat_number = self.pool.get("res.partner")._split_vat(self.browse(cr, uid, ids)[0].vat)
        vat_no = "'CC##' (CC=Country Code, ##=VAT Number)"
        if default_vat_check(vat_country, vat_number):
            vat_no = base_vat._ref_vat[vat_country] if vat_country in base_vat._ref_vat else vat_no
        return '\n' + _('This VAT number does not seem to be valid.\nNote: the expected format is %s') % vat_no

    _constraints = [(check_vat, _construct_constraint_msg, ["vat"])]
